#! /usr/bin/env python3

from flask import Flask, render_template
import db
app = Flask(__name__)
app.debug = True


dico = {}
dico[None] = ["rien", "pas grand chose", "sieste"]
dico["user1"] = ["rien", "pas grand chose", "sieste"]
dico["user2"] = ["rien", "pas grand chose", "sieste"]
dico["Robert"] = ["rien", "pas grand chose", "sieste"]


@app.route('/')
def home():
    return render_template(
        "home.html")

@app.route('/user/')
@app.route('/user/<name>')

def user(name=None):
    todo=["rien", "pas grand chose", "sieste"]
    database = db.DB()
    print('Hello')
	
    return render_template(
        "user.html",
        name=name,
        todo=database.get(name))
	
@app.route('/users')
def users():
    database = db.DB()
    return render_template(
        "users.html", data=database.users())

if __name__ == '__main__':
    app.run(port=9999)
    app.run(host="0.0.0.0")
